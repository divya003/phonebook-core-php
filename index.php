<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="style.css" >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">



<title>Phone Book</title>

</head>
<body >

<center>
<h2 >*** PHONE BOOK ***</h2>
 


<br>


<h3 >CONTACTS</h3>
</center>
<hr>
<div class="container">

			<center>
			<button   type="button" class="btn btn-success" id="add" data-toggle="modal" data-target="#addmodal" >ADD</button>
			</center>
			<br>
			<br>
			<div class="panel-body">
			<table id="tbl-phone" class="table table-bordered"  width="100%">
			<thead>
			<tr>
			<th style="text-align:center">No.</th>
			<th style="text-align:center">Name</th>
			<th style="text-align:center">Phone</th>
			<th style="text-align:center">Edit</th>
			<th style="text-align:center">Delete</th>
			</tr>
			</thead>
			<tbody></tbody>
			</table>	
			
			
			</div>
			<div class="modal fade" id="addmodal" >
			  <div class="modal-dialog">
    			<div class="modal-content" style="background-color: #33A6B1;">
    			  	<div class="modal-header">
      				<center>
        			<h3 class="modal-title" >ADD</h3>
        			</center>
        			<button type="button" class="close" data-dismiss="modal" >
          			<span>&times;</span>
        			</button>
      				</div>
      				
      				
      				<div class="modal-body">
        
					<center>				
					<div class="form-group">
					<label>Name</label>
					<input type="text" name="name" id="name" class="form-group" >
					</div>
	
					<div class="form-group">
					<label>Phone</label>
					<input type="text" name="phone"id="phone" class="form-group" maxlength="10" >
					</div>
			
				
		
					</center>

    				</div>
      				<div class="modal-footer">
        
       				 <div  style="text-align: center;"class="form-group">
					<button type="button"  class="btn btn-primary" id="save" name="save">SAVE</button>
					<button type="button" class="btn btn-secondary"  id="closebtn" class="close" data-dismiss="modal">Close</button>
					</div>
        			</div>
        		</div>
      		</div>
   </div>
		<div class="modal fade" id="editmodal" >
			  <div class="modal-dialog">
    			<div class="modal-content" style="background-color: #33A6B1;">
    			  	<div class="modal-header">
      				<center>
        			<h3 class="modal-title" >EDIT</h3>
        			</center>
        			<button type="button" class="close" data-dismiss="modal" >
          			<span>&times;</span>
        			</button>
      				</div>
      				
      				
      				<div class="modal-body">
        
					<center>				
					<div class="form-group">
					<input type="hidden" id="eid">
					<label>Name</label>
					<input type="text" name="ename" id="ename" class="form-group" >
					</div>
	
					<div class="form-group">
					<label>Phone</label>
					<input type="text" name="ephone"id="ephone" class="form-group" maxlength="10">
					
					</div>
			
				
		
					</center>

    				</div>
      				<div class="modal-footer">
        
       				 <div  style="text-align: center;"class="form-group">
					<button    class="btn bt-info" id="editbtn" >EDIT</button>
					<button type="button"  id="closebtn" class="btn btn-info" class="close" data-dismiss="modal">Close</button>
					</div>
        			</div>
        		</div>
      		</div>
   </div>
	
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script src="functions.js"></script>

</body>
</html>


