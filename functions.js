$("document").ready(function(){
	getAll();
});
//alert("ready");
	$("#save").click(function(){
		$.ajax({
			method:'post',
			url:'insert.php',
			dataType:'json',
			data:{
				name:$("#name").val(),
				phone:$("#phone").val(),
			},
			success:function(data)
			{
				getAll();
					alert(data.message);
					if(data.status=="success")
						{
						$("#name").val('');
						$("#phone").val('');
						$('#addmodal').hide();
						$('.modal-backdrop').hide();
					
						}
					
			},
			error:function()
				{
				alert("error ajax in insert");
				$("#addmodal").modal("hide");
				}
			
		});
		return false;
		
	});
	
	
	
	function getAll()
	{
		//alert("getAll working");
		//$("#tbl-phone tbody tr").remove();
		$.ajax({
			method:'GET',
			url:'read.php',
			dataType:'json',
			//data:'response',
			success:function(data)
			{
		
				var trHTML = '';
				var c=0;
				var no=1;
				$('#tbl-phone tbody tr').remove();
		        $.each(data, function (){ 
		        {
		        
		            trHTML += '<tr><td style="text-align:center">'+no+'</td><td style="text-align:center">' + data[c].name + '</td><td style="text-align:center">' + data[c].phone + '</td><td style="text-align:center"><button  class="btn bt-info" id="edit" data-toggle="modal" data-target="#editmodal" onclick=getDetails('+data[c].id+') >EDIT</button></td><td style="text-align:center"><button class="btn bt-info" id="delete" onclick="deletecontact('+data[c].id+')" >DELETE</button></td></tr>';
		            
		            no++;	
		            c++;
		        
		    	}
		        });
		        $('#tbl-phone').append(trHTML);
			},
			error:function()
				{
				alert("error ajax in read");
				}
		});
		return false;
	};
	function deletecontact(id)
	{
		$.ajax({
			method:'post',
			url:'delete.php',
			dataType:'json',
			data:{
				"id":id,
			},
			success:function(data)
			{
					getAll();
					alert(data.message);
					
				
			},
			error:function()
				{
				alert("error ajax in delete");
				}
			
		});
		return false;
		
	};
	function getDetails(id)
	{
		$.ajax({
			method:'post',
			url:'getdetails.php',
			dataType:'json',
			data:{
				"id":id,
			},
			success:function(data)
			{
				$("#ename").val(data.name);
				$("#ephone").val(data.phone);
				$("#eid").val(data.id);
			},
			error:function()
				{
				alert("error ajax in getDtails");
				}
			
		});
		return false;
	
	};
	
	$("#editbtn").click(function()
	{
		$.ajax({
			method:'POST',
			url:'edit.php',
			dataType:'json',
			data:{
				"id":$("#eid").val(),
				"name":$("#ename").val(),
				"phone":$("#ephone").val()
			},
			success:function(data)
			{
					getAll();
					alert(data.message);
					if(data.status=="success")
						{
						$("#eid").val('');
						$("#ename").val('');
						$("#ephone").val('');
						$('#editmodal').hide();
						$('.modal-backdrop').hide();
						
					}
					
			},
			error:function()
				{
				alert("error ajax in editcontact");
				}
			
		});
		return false;
		
	});
	
	
	


	

